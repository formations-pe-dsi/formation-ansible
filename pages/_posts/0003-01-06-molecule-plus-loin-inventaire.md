## Molecule - Instances

Il est possible de définir plus d’une instance à provisionner et sur lesquelles lancer les tests.

Cela permet notamment de :

  * Tester sur différents OS
  * Tester sur une infrastructure complexe

Chaque instance dispose d’un nom unique, de groupes (facultatifs), et de paramètres de création.

---

## Molecule - Tester sur plusieurs OS

Déclarer plusieurs entrées dans la section platforms permet à Molecule d’instancier des containers disposant d’images de base différentes

L’exécution se fera en parallèle sur tous les containers

```yaml
#molecule/default/molecule.yml:
---
dependency:
  name: galaxy
driver:
  name: docker
platforms:
  - name: centos8
    image: docker.io/pycontribs/centos:8
  - name: debian
    image: debian:jessie
provisioner:
  name: ansible
verifier:
  name: ansible
```

---

## Molecule - Inventaire - groupes

<pre><code class="lang-yaml hljs" style="float: left; margin-right: 40px; max-height: 400px; font-size: 0.8em;">#molecule/default/molecule.yml:
---
platforms:
  - name: master
    image: debian:jessie
    groups:
      - masters
  - name: standby1
    image: debian:jessie
    groups:
      - standby
  - name: standby2
    image: debian:jessie
    groups:
      - standby
</code></pre>
<br/>

Comme nous pouvons gérer un inventaire, il est nécessaire de pouvoir grouper, ce qui nous permettra par exemple, d’exécuter le rôle avec des paramètres différents sur différents groupes.

Imaginons que l’on souhaite tester un rôle postgresql qui peut configurer un cluster hot-standby, il nous faudra exécuter le rôle sur l’instance master en premier, puis sur les différents slaves.

---

## Molecule : Inventaire - variables

Chaque “platform” déclarée peut disposer de variables, ce sont les hosts_vars et group_vars d’ansible.

Cette déclaration se fait dans la section provisioner du molecule.yaml :

```yaml
provisioner:
  name: ansible
  inventory:
    host_vars:
      centos8:
        maVariable: saValeur
    group_vars:
      group1:
        autreVariable: autreValeur
      group2:
        variable: valeur
```

---

## Molecule - TP5: Gérer un inventaire

![TP](./images/TP.png)

💡 Scrollez vers le bas pour les détails du TP ( 1 page )

--

## TP5

🎯 L’objectif de ce TP est de disposer de variables d’inventaires dans notre rôle molecule,
nous permettant ainsi d’éxecuter des tests contextualisés.

Dans le fichier molecule/default/converge.yml :
* Modifier le dépot du fichier pour qu’il contienne une nouvelle ligne:
  * {% raw %}type_instance={{type_instance}}{% endraw %}

Dans le fichier molecule/default/molecule.yml:
* Dans la section platforms, ajouter :
  * 1 instance, nommée “maitre” et appartenant au groupe masters
  * 2 instances nommées “esclave[1,2]” appartenant au groupe slaves

Exemple :

```yaml
- name: nom_instance
  image: docker.io/pycontribs/centos:8
  pre_build_image: true
  groups:
    - groupe_de_l_instance
```

--

* Dans la section “provisioner”, définir la variable type_instance pour chaque groupe a maître ou esclave

Exemple:

```yaml
provisioner:
  name: ansible
  inventory:
    group_vars:
      nom_du_groupe:
        variable: valeur
```
Nous avons modifié le comportement de notre rôle, il faut donc mettre a jour les tests unitaires pour qu’ils vérifient le nouveau comportement

* Exécuter l’intégralité des tests : `molecule test`

---

## Molecule : Inventaire - A retenir.

![thinking](./images/thinking.png)

L’inventaire d’ansible prend une autre forme dans Molecule

Il est toujours possible de:
  * déclarer des groupes
  * déclarer des groupes de groupes

Les *group_vars* et *host_vars* peuvent être définies dans le fichier molecule.yml

---

## Molecule : Gérer des pré-requis système?

<img align="left" width="400" height="400" src="./images/question.png" style="border: none;margin-right: 20px;">
<br/>

Le rôle que nous testons pour le moment est très simple, et ne nécessite pas de prérequis spécifiques sur le système cible.

Dans le prochain TP, nous allons tester sur debian buster, qui ne dispose pas de python et ne peut donc pas exécuter de module ansible.

Comment, dans ce cas, exécuter notre rôle dans un container docker?
