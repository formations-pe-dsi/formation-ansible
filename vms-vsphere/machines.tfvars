machines = [
    {"name": "1-debian10-1", "template": "template-vm-debian12-x86-64", "user_id": "IUGI3000"},
    {"name": "1-alma9-2", "user_id": "IUGI3000"},
    {"name": "1-alma9-3", "user_id": "IUGI3000"},
    {"name": "2-debian10-1", "template": "template-vm-debian12-x86-64", "user_id": "IVMA4510"},
    {"name": "2-alma9-2", "user_id": "IVMA4510"},
    {"name": "2-alma9-3", "user_id": "IVMA4510"},
    {"name": "3-debian10-1", "template": "template-vm-debian12-x86-64", "user_id": "ILJE7680"},
    {"name": "3-alma9-2", "user_id": "ILJE7680"},
    {"name": "3-alma9-3", "user_id": "ILJE7680"},
    {"name": "4-debian10-1", "template": "template-vm-debian12-x86-64", "user_id": "IPLA7660"},
    {"name": "4-alma9-2", "user_id": "IPLA7660"},
    {"name": "4-alma9-3", "user_id": "IPLA7660"},
    {"name": "5-debian10-1", "template": "template-vm-debian12-x86-64", "user_id": "IJCO7870"},
    {"name": "5-alma9-2", "user_id": "IJCO7870"},
    {"name": "5-alma9-3", "user_id": "IJCO7870"}
]

solidserver_space  = "Pole Emploi"
solidserver_subnet = "ESXI_ADMIN_TDC"
solidserver_pool   = "TDC_POOL"