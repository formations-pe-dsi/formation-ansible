## Ansible : les boucles - utilisation de loop

➡ Les boucles permettent d’exécuter une action (module) plusieurs fois

➡ Utilisation de la directive loop, nouvelle directive recommandée depuis la version 2.5  

➡ with_items est l’ancienne syntaxe toujours compatible

➡ Utilisation du mot clé item pour accéder à l’élément d’une boucle

{%raw%}
Quelques exemples :
* en utilisant une liste prédéfinie :

```yaml
- debug: msg="{{ item }}"
  loop:
    - 1
    - 2
```

* en utilisant une variable :

```yaml
- set_fact: values=[1,2,3,4]
- debug: msg="{{ item }}"
  loop: "{{values}}"
```
{%endraw%}

Note:

Expliquer que item est la variable par défaut des boucles

---

## Ansible : les boucles - utilisation de `with_<lookup>`

Possibilité d’utiliser un lookup plugin dans une boucle avec la directive `with_<lookup>`

Exemple avec with_file:

```yaml
- debug: var=item
  with_file:
  - "/etc/hosts"
  - "/etc/resolv.conf"
```

Exemple avec with_sequence:

```yaml
- debug: msg="{{ item }}"
  with_sequence: start=1 end=5
```

Retrouver la liste des lookup plugins sur https://docs.ansible.com/ansible/latest/collections/index_lookup.html

Note:

L’exemple avec with_file est à détailler, le lookup plugin “file” retournant le contenu d’un fichier local, l’item contiendra à chaque tour de boucle le contenu d’un fichier.

Montrer plus d’exemples avec sequence en allant sur le site. 

---

## Ansible : les conditions

➡ Les conditions permettent d’ignorer ou de forcer l’exécution d’une action (module). 

➡ Utilisation de la directive when  avec différents opérateurs possibles:

  * opérateurs de comparaison https://jinja.palletsprojects.com/en/3.0.x/templates/#comparisons
  * opérateurs logiques https://jinja.palletsprojects.com/en/3.0.x/templates/#logic
  * autres opérateurs https://jinja.palletsprojects.com/en/3.0.x/templates/#other-operators

Exemple de condition basée sur une variable:

```yaml
vars:
  epic: true
tasks:
  - name: Run the command if "epic" is false
    ansible.builtin.shell: echo "This certainly isn't epic!"
    when: not epic
```

Exemple de condition basée sur un fact:

```yaml
when: ansible_distribution == "Debian"
```

Note:

When est un paramètre de la task, et non du module, d’où son indentation au niveau de la task et non des paramètres de module

Toute variable peut être utilisée, on peut conditionner une task sur le résultat de l’exécution d’une task précédente par exemple, en utilisant une variable définie via le “register”

{%raw%}
La syntaxe du when peut presque être considérée comme un exception, ici on passe directement une expression sans les {{ }} de jinja. Il est possible et souvent utile de rajouter des quotes autour de l’expression
{%endraw%}

---

## Ansible - TP4 : utiliser les boucles et les conditions

![TP](./images/TP.png)

💡 Scrollez vers le bas pour les détails du TP ( 2 pages )

Note:

A la racine du projet, les répertoires files et templates ont une signification particulière, ansible peut y trouver les fichiers passés en paramètres des modules en utilisant uniquement le nom du fichier

loop ou withfileglob :”*.cfg”

bien aligner le when avec le module de la tache que nous executons( le module etant aligné avec le nom de la tache)

La solution de ce TP dans le repository git est un exemple d’utilisation avancée des variables et conditions, si on en a le temps, il peut être intéressant de le parcourir avec les stagiaires

--

## TP4.1 Externaliser les variables de l’inventaire dans des fichiers 

🎯  L’objectif de ce TP est de créer un playbook avec des tasks utilisant des boucles et des conditions afin d’aller copier des fichiers de configurations sur vos machines cibles. 

💡 Le répertoire `files` est un répertoire reconnu par défaut par Ansible. Ansible viendra automatiquement chercher dans ce répertoire, les fichiers que l’on souhaite copier sur nos machines. 

  1. Créer à la racine de votre projet, un répertoire nommé files contenant 2 fichiers vides nommés :

  - configuration1.cfg 
  - configuration2.cfg

  2. Créer à la racine de votre projet, un nouveau playbook nommé init-configuration.yml avec un play nommé "Copier des fichiers de configuration" ayant pour cible tous les hôtes de votre inventaire. 

--

## TP4.2


Pour la création des tasks suivantes, pensez à utiliser les conditions avec des facts, les boucles et des modules permettant de manipuler des fichiers / répertoires. 
[N’oubliez pas, sous unix, tout est fichier, même les répertoires](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/index.html#modules). 

1. Ajouter à votre play, une task permettant de créer le répertoire /etc/ma_conf pour les machines Debian. 
1. Ajouter une task permettant de créer le répertoire /opt/ma_conf pour les machines AlmaLinux. 
1. Ajouter une task permettant de créer un fichier vide nommé alma_flag dans le répertoire de configuration /opt/ma_conf pour les machines AlmaLinux.   
1. Ajouter une task permettant de copier les fichiers de configuration configuration1.cfg et configuration2.cfg dans le répertoire /etc/ma_conf pour les machines Debian. 
1. Ajouter une task permettant de copier les fichiers de configuration configuration1.cfg et configuration2.cfg dans le répertoire /opt/ma_conf pour les machines AlmaLinux. 
1. Une fois l’implémentation terminée, jouer votre playbook init-configuration.yml. 
1. Vous pouvez vous connecter en ssh sur vos machines cibles pour vérifier que Ansible a bien fait son travail. 
1. Exécutez à nouveau le playbook, que constatez-vous ? 

---

## Ansible - TP4 : debrief

<img align="left" width="400" height="400" src="./images/question.png" style="border: none;margin-right: 20px;">
<br/>

La task “Création du fichier flag sur machine CentOS” utilisant le module file avec le paramètre “state: touch” se termine toujours par un statut <span style="color: yellow;">changed=1</span>. 

  * Pourquoi ?
  * Comment rendre la task indempotent ?

Note:

Par défaut, state: touch change la date d’accès et de modification du fichier. Donc même si rien n’a vraiment changé sur le fichier, le résultat de la task sera à changed.

Demander aux participants d’essayer de trouver une solution pour donner un comportement indempotent à cette task en regardant les paramètres du module file.

Solution : il est possible d’ajouter sur le module file les paramètres :
  * access_time: preserve
  * modification_time: preserve