## Ansible : pour aller plus loin...

![plus_loin](./images/plus_loin.png)

---

## Ansible : les handlers

**Handler**: task dont l’exécution est notifiée sur une autre task et qui sera déclenchée par défaut à la fin de l’exécution d’un play si la task portant le handler a effectué un changement sur l’hôte.

  * Déclarer le déclenchement d’ un handler sur une task avec le mot clé notify

  ```yaml
  - name: Template config file
    ansible.builtin.template:
      src: template.j2
      dest: /etc/foo.conf
    notify:
      - Restart memcached
  ```

  * Déclarer un handler : 

<table style="width: 90%;">
<tr>
<th>Dans un playbook</th>
<th>Dans un role</th>
</tr>
<tr>
<td>
<pre><code class="lang-yaml hljs">handlers:
  - name: Restart memcached
    ansible.builtin.service:
      name: memcached
      state: restarted
</code></pre>
</td>
<td>
<pre><code class="lang-yaml hljs"># fichier mon_role/handlers/main.yml
- name: Restart memcached
  ansible.builtin.service:
    name: memcached
    state: restarted
</code></pre>
</td>
</tr>
</table>

Note:

Une modification du fichier de configuration d’un service peut nécessiter le redémarrage du service pour que la configuration modifiée prenne effet.

Cela évite dans certains cas de démarrer plusieurs fois un service dans un play.

Le déclenchement d’un handler se fait par défaut à la fin d’un play mais il est possible de lancer une task dite “flush” pour lancer les handlers qui auront été notifiés avant le flush

https://docs.ansible.com/ansible/latest/user_guide/playbooks_handlers.html#controlling-when-handlers-run

---

## Ansible : les handlers - flush

Possibilité de forcer le déclenchement des handlers en utilisant une task dite “flush”.

Exemple:

```yaml
tasks:
 - name: Some tasks go here
   ansible.builtin.shell: …
   notify: name handler

 - name: Flush handlers
   meta: flush_handlers

- name: Some other tasks
   ansible.builtin.shell: ...
```

Seuls les handlers notifiés avant la task flush seront déclenchés.

Note:

Le déclenchement d’un handler se fait par défaut à la fin d’un play mais il est possible de lancer une task dite “flush” pour lancer les handlers qui auront été notifiés avant le flush

https://docs.ansible.com/ansible/latest/user_guide/playbooks_handlers.html#controlling-when-handlers-run

---

## Ansible - TP8 :  utiliser les handlers


![TP](./images/TP.png)

💡 Scrollez vers le bas pour les détails du TP ( 2 pages )

--

## TP8.1

🎯 L’objectif de ce TP est de modifier la configuration de votre serveur web dans le rôle “apache” que vous avez créé précédemment et de déclarer un handler au sein de ce rôle pour redémarrer le service.

1. Dans votre répertoire roles/apache, créer un répertoire handlers et créer dans ce répertoire un fichier main.yml
1. Dans ce fichier main.yml, ajouter une task “handler” qui permettra de redémarrer le service Apache que ce soit sur Debian ou sur AlmaLinux
1. Dans le fichier des tasks de votre rôle, ajouter avant la task permettant de s’assurer que le service Apache fonctionne bien, une task permettant de modifier le port d’écoute par défaut 80 par le port 8080 ( directive Listen ).

    💡 Vous pouvez utiliser le [module lineinfile](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/lineinfile_module.html)

    Fichier de configuration pour AlmaLinux : /etc/httpd/conf/httpd.conf

    Fichier de configuration pour Debian : /etc/apache2/ports.conf

4. Dans cette task, ajoutez une notification sur le handler créé
4. Jouer votre playbook install-apache.yml

--

## TP8.2

1. Vérifier que vos pages web sont accessibles sur les deux machines
1. Rejouer votre playbook, que constatez-vous ?
1. Ajouter une task permettant de déclencher votre handler avant l’exécution de la task permettant de s’assurer que le service Apache fonctionne bien.
1. Rejouer votre playbook

---

## Ansible : escalade de privilèges

L’escalade de privilèges permet d'exécuter des tâches avec des privilèges root ou avec d’autres permissions accordées à un utilisateur particulier.

  * **become** : permet d’activer l’escalade de privilèges. Cette directive peut se positionner sur un play ou sur une task.
  * **become_user** : permet d’indiquer l’utilisation d’ un user avec des droits particuliers. Par défaut, sa valeur est root.

Exemple :

```yaml
- name: Ensure the httpd service is running
  service:
    name: httpd
    state: started
  become: true
- name: Run a command as the apache user
  command: somecommand
  become: true
  become_user: apache
```

possibilité de préciser un mot de passe pour sudo avec l’option --ask-become-pass ou -K : `ansible-playbook -K nom du fichier playbook`

---

## Ansible : Vault

**Vault**: fonctionnalité dans Ansible permettant de chiffrer des données sensibles.

Quelques commandes :

  * Créer un fichier chiffré : `ansible-vault create **fichier chiffré à créer**`
  * Chiffrer un fichier existant : `ansible-vault encrypt **fichier à chiffrer**`
  * Editer un fichier chiffré : ̀`ansible-vault edit  **fichier chiffré**`
  * Consulter un fichier chiffré : `ansible-vault view  **fichier chiffré**`

Quelques options :

  * **--ask-vault-pass** : obtenir un prompt de saisie du password sur une commande ansible ou ansible-playbook
  * **--vault-password-file** : indiquer un fichier contenant le password Vault
    * attention de ne pas stocker le password en clair dans un fichier .txt
    * alternative, utiliser un programme (bash, python, etc…) permettant de récupérer le password stocké dans un endroit sécurisé (ex: <u>pass</u>  ou <u>HashiCorp Vault</u>) 