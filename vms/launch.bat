rem set http_proxy=http://proxyaws.pole-emploi.intra:8080
rem set https_proxy=http://proxyaws.pole-emploi.intra:8080
set http_proxy=http://gateway.zscaler.net:443
set https_proxy=http://gateway.zscaler.net:443

vagrant plugin install vagrant-proxyconf

set VAGRANT_HTTP_PROXY=%http_proxy%
set VAGRANT_HTTPS_PROXY=%https_proxy%
set VAGRANT_NO_PROXY="localhost,127.0.0.1,.pole-emploi.intra,172.17.10.100,172.17.10.101,172.17.10.102,172.17.10.103,172.17.10.50"

vagrant destroy --force
vagrant box update
vagrant up

@echo "fin de la procedure"
pause