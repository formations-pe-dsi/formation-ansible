
## Ansible : quelques rappels sur le YAML

```yaml
# An employee record
name: Martin D'vloper
job: Developer
skill: Elite
employed: true
unemployed: no
foods:
  - Apple
  - Orange
  - Strawberry
  - Mango
languages:
  perl: Elite
  python: Elite
  pascal: Lame
education: |
  4 GCSEs
  3 A-Levels
  BSc in the Internet of Things
```

🚯 Respectez une indentation homogène d’un ou plusieurs espaces sans quoi votre structure ne sera pas compréhensible.

Note:

  * un boolean peut être représenté par true/false ou yes/no
  * Tous les éléments d’une liste sont indentés au même niveau et commence avec un “ - “
  * Un dictionnaire est représenté par le couple “ clé: value” avec les 2 points suivis par un espace


Dans l'exemple ci contre 
  * le couple name: Martin est un dictionnaire (key, value)  la clé est terminée par deux points suivi d'un espace. Si pas respecté cela ne fonctionnera pas.
  * Foods représente une liste. Chaque donnée de la liste est itérée de 2 espaces, suivit du signe moins suivi d'un espace
  * Langages: représente une liste de dictionnaire.
  * Possibilité de répartir les données sur plusieurs lignes en utilisant le signe pipe

---

## Ansible : command line - lancer une tâche ponctuelle

Possibilité de lancer une tâche ponctuelle en utilisant directement la commande ansible :

| description commande | exemple d'utilisation |
|----------------------|-----------------------|
| ansible <hosts> -a <command> | ansible web1 -a “/sbin/reboot” |
| ansible <hosts> -m <module> | ansible web2 -m ping |

Bien que les commandes Ansible soient utiles pour des opérations simples, elles ne conviennent pas aux scénarios complexes.

Note:

La définition d’un module sera vue dans les slides suivants.

l’option -e permet également de fournir des arguments complémentaires à l'option -m (module)

---

## Ansible : fichier inventaire - hosts

Le fichier inventaire, au format YAML (autres formats possibles : INI ou TOML), liste les machines cibles sur lesquelles Ansible pourra se connecter et jouer les instructions décrites dans le playbook.

```yaml
#extrait fichier inventaire au format YAML
#[...]
web1:
  ansible_host: 172.17.10.101 
  ansible_user: root
web2: 
  ansible_host: 172.17.10.102 
  ansible_user: root
```

Quelques paramètres possibles :
  * ansible_connection (ssh, winrm, localhost)
  * ansible_host
  * ansible_user
  * ansible_port

https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#connecting-to-hosts-behavioral-inventory-parameters

Note:

La connexion aux hosts est effectuée avec SSH, ansible s’appuie donc sur la configuration de la machine maître, il utilise les clés SSH, le fichier config ssh et les known_hosts de l’utilisateur exécutant les commandes

Il est possible de spécifier des paramètres de connexion via :
  * des variables ansible
  * le fichier ansible.cfg

Il est également possible de demander à ansible de se connecter avec un utilisateur et d’utiliser “sudo” sur la machine cible. Ce paramétrage est hors scope de ce module, il fait partie des slides “pour aller plus loin”

---

## Ansible : fichier inventaire - groupes

Les groupes permettent d'exécuter des actions sur plusieurs cibles en parallèle. Un groupe spécial existe : le groupe **all**, il contient l’intégralité des cibles.

Il est possible de regrouper des groupes dans un groupe parent, en utilisatant le mot clé **children**.

<table>
<tr>
<td>
<pre><code class="lang-yaml hljs" style="width: 300px; font-size: 0.8em;" >#structure fichier inventaire
group_name :
   children:
      group_name:
         hosts:
           host_name:
              variable_name: value
           host_name: 
              variable_name: value
      group_name: 
         hosts: 
            host_name:
               variable_name: value
</code></pre>
</td>
<td>
<pre><code class="lang-yaml hljs" style="width: 300px; font-size: 0.8em;">#exemple fichier inventaire
application :
   children:
      web1:
         hosts:
            apache1:
               ansible_host: 10.0.2.100
            apache2: 
               ansible_host: 10.0.2.101
      database: 
         hosts: 
            postgres:
               ansible_host: 10.0.2.102
</code></pre>
</td>
</tr>
</table>

Il est possible de lister les hôtes d’un groupe en utilisant la commande ansible et l’option list-hosts 

Exemple pour lister tous les hôtes d’un inventaire: `ansible all --list-hosts`

Note:

La structure du fichier:
  * Le mot clé “hosts” permet de définir les host d’un group
  * Le mot clé “children” permet de définir les sous-groupes d’un groupe parent
  * Le mot clé “vars” permet de définir des variables de groupe ( appliquées à tous les hosts du groupe )
  * Des variables peuvent être ajoutées aux hosts

https://linux.goffinet.org/ansible/comprendre-inventaire-ansible/#11-d%C3%A9finition-dun-inventaire-ansible

---

## Ansible : fichier inventaire - exemple format INI 

```ini
#fichier inventaire au format INI
apache1 ansible_host=10.0.2.100
apache2 ansible_host=10.0.2.101
postgres ansible_host=10.0.2.102

[web]
apache1
apache2

[backend]
postgres

[application:children]
web
backend
```

Note:

La structure du fichier:
  * Les premières lignes sont nos hosts, ansible s’appuie normalement sur leur nom pour se connecter, s’il n’y a pas d’enregistrement DNS pour le host, il est possible d’utiliser la variable ansible_host
  * Les groupes contiennent une liste d’hosts, il est possible de rajouter des paramètres sur les hosts a cet endroit
  * Le mot clé “:children” permet de définir les sous-groupes d’un groupe parent
  * Le mot clé “:vars” permet de définir des variables de groupe ( appliquées à tous les hosts du groupe )

---

## Ansible - TP1 : créer son premier inventaire 

![TP](./images/TP.png)

💡 Scrollez vers le bas pour les détails du TP ( 2 pages )

Note:

La création du fichier ansible.cfg n’est pas obligatoire, elle nous permet de simplifier les commandes suivantes, en effet, l’inventaire est défini par défaut dans ce fichier de configuration et l’on peut donc se passer du paramètre “-i chemin/du/fichier_inventaire” des commandes ansible*

Ce paramètre -i permet de disposer de plusieurs inventaires pour un même playbook, pour par exemple gérer plusieurs environnements.

Le fichier de configuration ansible permet de définir d’autres éléments du comportement, comme la clé ssh à utiliser, la vérification des “host keys” par ssh et leur enregistrement dans le known_hosts

--

## TP1.1

🎯  L’objectif de ce TP est de créer un inventaire qui permettra de définir les hôtes sur lesquels vos playbooks agiront.

ℹ Par défaut, Ansible ira chercher un fichier inventaire présent sous `/etc/ansible/hosts`. Il est possible de surcharger cette configuration par défaut en définissant un fichier `ansible.cfg` dans son projet.  

  1.  Dans votre projet mon-playbook-ansible, créer un fichier inventaire nommé hosts.yml  

Exemple : 
```yaml
application:
  children:
    web1:
        hosts:
          apache1:
              ansible_host: 10.0.2.100
          apache2:
              ansible_host: 10.0.2.101
    database:
        hosts:
          postgres:
              ansible_host: 10.0.2.102
```

--

## TP1.2

  2. Dans ce fichier hosts.yml : 
    * ajouter les cibles ansible-cible-1 et ansible-cible-2 dans un groupe nommé groupe1 
    * ajouter la cible ansible-cible-3 dans un groupe nommé groupe2 
    * créer le groupe serveurs_linux englobant les 2 groupes groupe1 et groupe2 

  3. Attribuer les variable `ansible_user: root` et `ansible_password: root` sur chacun des hosts 

  4. Attribuer la variable ansible_host: `<numero>-<distribution>-<numero-cible>.formationansible.sii24.pole-emploi.intra` sur chacun des hosts. ( exemple : 1-debian10-1.formationansible.sii24.pole-emploi.intra ). La machine debian doit être ansible-cible-1.

  5. Créer un fichier ansible.cfg avec les options suivantes : 

```ini
[defaults] 
inventory = hosts.yml 
host_key_checking = false 
```

  6. Executer la commande `ansible -m ping all`  

Retrouvez plus d’informations sur les différentes options possibles dans la [documentation officielle](https://docs.ansible.com/ansible/latest/reference_appendices/config.html#ansible-configuration-settings)