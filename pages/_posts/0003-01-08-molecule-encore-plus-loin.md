## Molecule : Pour aller (encore) plus loin - testinfra

Les tests unitaires avec ansible permettent de tester facilement, en utilisant le même langage que le rôle.

Cependant, le fichier de tests devient vite verbeux, et la complexité peut rapidement augmenter.

Il existe des alternatives pour exécuter des tests, notamment testinfra, qui est un framework python de test d’infrastructure.

Exemple de test avec tesinfra :

```python
def test_containerd_installed(host):
   containerd = host.file("/usr/bin/containerd")
   assert containerd.exists
   assert containerd.user == "root"
   assert containerd.group == "root"
   assert containerd.mode == 0o755
```

---

## Molecule : Pour aller (encore) plus loin - vagrant

Docker nous permet de tester rapidement, cependant, dans certains cas, l’utilisation de machines virtuelles est souhaitable.

Il existe un driver vagrant pour Molecule, qui permet de s’interfacer facilement avec virtualbox par exemple

Exemple d’une configuration instanciant sur virtualbox une machine virtuelle fedora 32 disposant de 512M de ram et 1 CPU :

```yaml
driver:
  name: vagrant

platforms:
  - name: instance
    box: fedora/32-cloud-base
    memory: 512
    cpus: 1
```

---

## Molecule : Pour aller (encore) plus loin - libvirt

Vagrant nous permet d’instancier des machines virtuelles sur le poste de dev, cependant les machines virtuelles sont consommatrices de ressources, et si nous souhaitons tester sur une infra conséquente, mieux vaudrait pouvoir déporter l’exécution.

Le connecteur libvirt de vagrant permet de se connecter par exemple à qemu/kvm ou encore à vmware pour l’exécution des machines virtuelles.

Exemple d’utilisation avec connection à un host kvm distant :

```yaml
driver:
  name: vagrant
  provider:
    name: libvirt

platforms:
  - name: fedora
    box: fedora/32-cloud-base
    memory: 1024
    cpus: 1
    provider_options:
      connect_via_ssh: ${REMOTE_HOST:-false}
    provider_raw_config_args: ['host="kvm"', 'username="libvirt-user"']
```

---

## Molecule : Pour aller (encore) plus loin - CI ( gitlab )

Les tests Molecule sont intégrables dans toute intégration continue.

Exemple avec gitlab ci : 

```yaml
image: quay.io/ansible/molecule:latest
services:
  - docker:dind

stages:
  - tests

molecule-role-common:
  stage: tests
  tags:
    - docker
  variables:
    DOCKER_HOST: "tcp://docker:2375"
  script:
    - molecule test
```

---

## Molecule - objectifs atteints ?

<img align="left" src="./images/tick_guy.png" style="border: none;margin-right: 40px;">
<br/>

A l’issue de ce module, vous êtes maintenant en mesure de :

  * Tester vos rôles Ansible pour s’assurer de leur bon fonctionnement
  * Vérifier les bonnes pratiques d'écriture de vos rôles

---

## Molecule : Dockerfile et paramètres de docker

![thinking](./images/thinking.png)

Molecule permet donc :

* de tester localement ses rôles ansible
* d'intégrer les tests dans une intégration continue

Les tests sont exécutables dans docker ou sur des machines virtuelles

Les infrastructures éphémères gérées par Molecule permettent de valider rapidement des modifications de code

=> L’utilisation de docker permet d’accélérer encore les tests locaux

Il est possible de gérer un inventaire et des variables, et donc de tester des infrastructures complexes (exemple : installation de clusters kubernetes multi-masters)

Les tests permettent de s’assurer que le rôle aura bien le comportement attendu lors d’une livraison sur un environnement final

---

## Molecule : Quelques exemples

Des exemples de rôles et collections testées par Molecule 

En interne Pôle-emploi :

  * https://gitlab.com/incubateur-pe/containerd Installation de containerd
  * https://gitlab.com/incubateur-pe/crio Installation de cri-o
  * https://gitlab.com/incubateur-pe/kubernetes-bare-metal Création de clusters kubernetes
  * https://git-scm.pole-emploi.intra/labo/ansible/configuration-bare-metal Configuration des vms vsphere

Quelques exemples notables sur ansible-galaxy :

  * https://galaxy.ansible.com/geerlingguy Jeff Geerling : tous ses rôles sont testés par Molecule
  * https://galaxy.ansible.com/oasis_roles/ Une collection développée par redhat
  * https://github.com/kubernetes-sigs/kubespray Le couteau suisse du déploiement k8s