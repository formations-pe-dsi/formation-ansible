data "vsphere_datacenter" "datacenter" {
  name = var.vsphere_datacenter
}

data "vsphere_datastore" "datastore" {
  name          = var.vsphere_datastore
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

data "vsphere_compute_cluster" "cluster" {
  name          = var.vsphere_cluster
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

data "vsphere_network" "network" {
  name          = var.vsphere_network
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

data "vsphere_virtual_machine" "template" {
  for_each = { for machine in var.machines :  machine.template => machine...}

  name          = each.key
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

resource "vsphere_folder" "root_formation" {
  path          = "formation"
  type          = "vm"
  datacenter_id = "${data.vsphere_datacenter.datacenter.id}"
}

resource "vsphere_folder" "uid" {
  for_each = { for machine in var.machines :  machine.user_id => machine...}

  path          = "${vsphere_folder.root_formation.path}/${each.key}"
  type          = "vm"
  datacenter_id = "${data.vsphere_datacenter.datacenter.id}"
}

resource "solidserver_ip_address" "ips" {
  for_each = { for machine in var.machines :  machine.name => machine}

  space  = var.solidserver_space
  subnet = var.solidserver_subnet
  pool   = var.solidserver_pool
  name   = "${each.key}.formationansible.sii24.pole-emploi.intra"
}

resource "macaddress" "macs" {
  for_each = { for machine in var.machines :  machine.name => machine}
}

resource "vsphere_virtual_machine" "vm" {
  for_each = { for machine in var.machines :  machine.name => machine}

  name             = each.key
  resource_pool_id = data.vsphere_compute_cluster.cluster.resource_pool_id
  datastore_id     = data.vsphere_datastore.datastore.id
  num_cpus         = each.value.num_cpus
  memory           = each.value.memory
  guest_id         = data.vsphere_virtual_machine.template[each.value.template].guest_id
  folder           = vsphere_folder.uid[each.value.user_id].path
  firmware         = data.vsphere_virtual_machine.template[each.value.template].firmware
  network_interface {
    network_id      = data.vsphere_network.network.id
    use_static_mac  = true
    mac_address     = upper(macaddress.macs[each.key].address)
  }
  disk {
    label            = "disk0"
    size             = data.vsphere_virtual_machine.template[each.value.template].disks.0.size
    thin_provisioned = data.vsphere_virtual_machine.template[each.value.template].disks.0.thin_provisioned
  }
  clone {
    template_uuid = data.vsphere_virtual_machine.template[each.value.template].id
  }
  extra_config = {
    "guestinfo.userdata"            = filebase64("${path.module}/userdata.yaml")
    "guestinfo.userdata.encoding"   = "base64"
    "guestinfo.metadata"            = base64encode(templatefile("${path.module}/metadata.tftpl", {ip = solidserver_ip_address.ips[each.key].address, name = each.key, mac = lower(macaddress.macs[each.key].address)}))
    "guestinfo.metadata.encoding"   = "base64"
  }
}
