## molecule : pour aller plus loin...

![plus_loin](./images/plus_loin.png)

---

## Et le TDD ?

Le TDD (Test Driven Development) est une méthode de développement logiciel dans laquelle l'écriture des tests automatisés dirigent l'écriture du code source.

On inverse donc le paradigme “naturel” :

<div class="mermaid">
  <pre>
graph LR
  A(Ajouter un test) --> B(Executer le test et constater un echec)
  B --> C(Ajouter du code pour faire passer le test)
  C --> D(Executer le test et constater qu'il passe)
  D --> E(Remaniement du code)
  E --> F(Controler que les tests passent toujours)
  F --> A
  </pre>
</div>

Note:

L’objectif n’est pas de présenter dans le détail le TDD pour que les stagiaires l’applique en fin de formation, mais de faire le lien avec la formation TDD pour ceux qui souhaitent aller plus loin.

---

## Molecule : Dépendances et état attendu de l’infra

<img align="left" width="400" height="400" src="./images/question.png" style="border: none;margin-right: 20px;">
<br/>

Molecule nous permet de tester un rôle. Mais ce rôle peut avoir des dépendances sur d’autres rôles, ou encore ne peut être utilisable que si l’infrastructure cible dispose déjà d’un certain niveau de configuration.

<u>Exemple:</u>

Nous souhaitons tester un rôle qui déploie des fichiers web statiques.
Ce rôle a donc besoin d’une infrastructure disposant d’un serveur web.

Comment allons-nous disposer de cette infrastructure pour nos tests?

---

## Molecule - dépendances locales

Molecule permet de gérer les dépendances des tests à installer sur l'hôte local

Deux types de gestionnaire de dépendances existent :
  * galaxy : rôles et collections
  * shell : tout ce que galaxy ne saurait pas faire (apt-get install par exemple)

Shell est à utiliser avec parcimonie, il implique potentiellement des modifications de l’hôte, et est dépendant de sa distribution (non portable)

Exemple galaxy:

```yaml
dependency:
  name: galaxy
  options:
    role-file: molecule/commun/requirements.yml  
    requirements-file: molecule/commun/collections.yml
```

Exemple Shell:

```yaml
dependency:
  name: shell
  command: apt-get install curl
```

Note:

Si deux dependency sont déclarés dans le molecule.yml, seul le dernier sera pris en compte

Ces dépendances concernent le rôle à tester, elles ne doivent pas servir à installer docker par exemple

---

## Molecule : dépendances - requirements.yml

Le gestionnaire de dépendances galaxy utilise un fichier requirements.yml dont le format dépend de l’option utilisée.
  * role-file : Liste de roles galaxy (peu ou plus utile)
  * requirements-file : Liste de rôles et collections (à privilégier)

Exemple de fichier requirements :

```yaml
#fichier requirements.yml
roles:
  - dep.role1 
  - dep.role2
collections:
  - ns.collection 
  - ns2.collection2
```

Note:

Le format du requirements-file est strictement identique au fichier de requirements galaxy

---

## Molecule - TP3: Gérer les dépendances

![TP](./images/TP.png)

💡 Scrollez vers le bas pour les détails du TP ( 2 pages )

--

## TP3

🎯 L’objectif de ce TP est de configurer molecule pour qu’il récupère les dépendances de notre rôle automatiquement, afin qu’il puisse s'exécuter avec tout l’environnement nécessaire

Dans le fichier molecule/default/requirements.yml, ajouter :
* Une dépendance sur le rôle galaxy “ugiraud.create_user”

Dans le fichier molecule/default/molecule.yml:
* Vérifier la présence de la section :
```yaml
dependency:
 name: galaxy
```

Dans le fichier molecule/default/converge.yml :
* Créer un utilisateur avec cette dépendance, nommé “monApplication”
* Modifier la création du fichier de configuration pour qu’il :
  * soit dans le home directory de l’utilisateur nouvellement créé
  * appartienne à l’utilisateur et groupe créés.
* Exécuter le playbook converge.yml qui va lancer l’installation : `molecule converge`

--

Dans le fichier molecule/default/verify.yml :
* Modifier la récupération du fichier pour qu’il soit récupéré depuis le home directory du nouvel utilisateur
* Modifier la task stat pour qu’elle pointe vers le nouveau chemin du fichier
* Modifier la vérification du fichier pour qu’elle vérifie l’appartenance à l'utilisateur et groupe créé
* Exécuter le playbook verify.yml qui va lancer vos tests unitaires : `molecule verify`

👍 Notre rôle est maintenant testé en récupérant ses dépendances à chaque lancement des tests. Il serait donc maintenant possible d’intégrer les tests dans un processus d'intégration continue