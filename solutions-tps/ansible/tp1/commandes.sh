# Lister tous les hôtes du groupe serveur_linux 
ansible serveurs_linux --list-host
# Envoyer une commande ping sur toutes les machines cibles
ansible -m ping serveurs_linux
# Mesurer la consommation mémoire des machines du groupe1
ansible -a "free" groupe1