# Ansible : galaxy 
## utiliser les ressources de la communauté

![ansible-galaxy](./images/galaxy.png)

---

## Ansible Galaxy : qu’est ce que c’est ?

Ansible Galaxy permet d’étendre les fonctionnalités d’Ansible en permettant à la communauté de publier des
ressources sur galaxy.ansible.com, les rendant disponibles à tout utilisateur d’Ansible.

Ces ressources peuvent être de type :
  * module
  * role
  * plugin
  * collection

Ansible Galaxy permet aussi de récupérer directement des ressources sur git.
Accéder au hub de la communauté sur https://galaxy.ansible.com/

---

## Ansible : galaxy - outil de ligne de commande

| <span style="color: green;">description commande</span> | <span style="color: blue;">exemple d’utilisation</span> |
|---------------------------|-------------------------------|
| Rechercher une ressource sur Ansible Galaxy<br/> `ansible-galaxy search<termes>` |  <br/>`ansible-galaxy search docker` |
| Créer un rôle avec l’arborescence normalisée<br/>`ansible-galaxy init <nom_role>` |  <br/>`ansible-galaxy init mon_role` |
| Installer un rôle depuis Ansible Galaxy<br/>`ansible-galaxy role install <nom_role>` |  <br/>`ansible-galaxy role install geerlingguy.docker` |
| Installer une collection depuis Ansible Galaxy<br/> `ansible-galaxy collection install <collection>` |  <br/>`ansible-galaxy collection install geerlingguy.php_roles` |
| Lister les rôles et collections installées<br/> `ansible-galaxy <role / collection> list` |  <br/>`ansible-galaxy role list` |
| Obtenir des détails sur un rôle installé<br/> `ansible-galaxy role info <namespace.nom>` |  <br/>`ansible-galaxy role info geerlingguy.docker` |

Note:

Depuis Ansible Galaxy, le processus de recherche ou d'importation se base sur les métadonnées trouvées dans le fichier meta/main.yml 

---

## Ansible : galaxy - namespaces

* Les namespaces sont séparés par des “.”
* Le premier niveau de namespace est le nom de l’auteur
* Dans les collections, un deuxième niveau de namespace est présent

Lors des recherches ( ansible-galaxy search ou sur https://galaxy.ansible.com ), il est possible de rechercher sur les auteurs

Exemple :

<div class="mermaid">
  <pre>
graph TD
  A[kubernetes.core.k8s_info] --> B[Auteur]
  A --> C[Collection]
  A --> D[Module/plugin/role]
  </pre>
</div>

Documentation officielle : https://docs.ansible.com/ansible/latest/cli/ansible-galaxy.html


---

## Ansible : galaxy - TP1: utiliser les rôles de la communauté (~10 min.)

![TP](./images/TP.png)

💡 Scrollez vers le bas pour les détails du TP ( 3 pages )

Note:

Le host ansible-cible-1 est sorti de la livraison car debian et centos disposent de versions différentes de pacemaker, empêchant la création du cluster

Quelques commandes pacemaker si les stagiaires souhaitent tester leur cluster :
  * pcs status => état du cluster
  * pcs resource => liste des resources
  * pcs resource colocation => configuration de co-localisation des ressources.

Un exercice intéressant peut être d’accéder à l’adresse fournie, puis d'arrêter le serveur du cluster qui porte l’IP virtuelle et de constater que la page est toujours disponible. vagrant halt nom_du_serveur et vagrant up nom_du_serveur par exemple

--

## TP1

🎯 L’objectif de ce TP est d’utiliser des ressources de la communauté Ansible Galaxy pour mettre en place sur vos 2 machines cibles AlmaLinux un serveur web apache, une adresse IP virtuelle devant le service apache et un cluster haute disponibilité avec pacemaker pour gérer l’état des ressources et basculer l’IP si besoin


1. Installer le rôle **geerlingguy.apache** en utilisant la commande ansible-galaxy install

2. De la même façon, installer le rôle **incubateurpe.pacemaker**

    💡 Par défaut, Ansible installe les rôles d’Ansible Galaxy sous **$HOME/.ansible/roles**. Vous
    pouvez choisir un autre emplacement en utilisant le paramètre *--roles-path*
    geerlingguy fait partie des top contributeurs de la communauté Ansible.


3. Si vous êtes curieux, consulter les sources des 2 rôles sur GitHub :
  - https://github.com/geerlingguy/ansible-role-apache
  - https://github.com/incubateur-pe/pacemaker

4. Vous pouvez voir la liste des rôles Galaxy installés sur votre machine avec la commande
  `ansible-galaxy list`

5. À la racine de votre projet, créer un playbook **cluster-haute-dispo.yml** avec un play nommé “Créer un cluster haute dispo” s’exécutant sur tous les hosts sauf **ansible-cible-1**.

--

6. Appeler le rôle **geerlingguy.apache** depuis ce play

7. Appeler le rôle **incubateurpe.pacemaker**

8. Ajouter sur le rôle **incubateurpe.pacemaker**, les variables suivantes :

```yaml
- role: incubateurpe.pacemaker
  vars:
    pcs_prometheus_exporter: false
    pcs_service_resources:
      - name: httpd
        prefix: Service_
        options: op monitor interval=5s
        clone: true
    pcs_virtual_ips:
      - name: VirtualIP0
        ip: <ip fournie>
    pcs_colocations:
      - resource1: VirtualIP0
        resource2: Service_httpd-clone
        ordered: true
  environment:
    no_proxy: <ip_fournie>,ansible-cible-2,ansible-cible-3
```

--

9. Exécuter votre playbook **cluster-haute-dispo.yml**

10. Votre page web est maintenant disponible en haute dispo sur http://*ip_fournie*

---

## Ansible : galaxy - Accélérer grâce à la communauté

<img align="left" width="400" height="400" src="./images/question.png" style="border: none;margin-right: 20px;">
<br/>

Imaginons que vous auriez dû écrire ce playbook,
combien de temps vous aurai-t-il fallu ?

---

## Ansible : galaxy - Gestion des versions des dépendances

<img align="left" width="400" height="400" src="./images/question.png" style="border: none;margin-right: 20px;">
<br/>

Nous avons installés 2 rôles en dernière version avec la commande
ansible-galaxy.

Est-ce que cela vous semble être une bonne pratique ?

Comment pourrait-on faire mieux ?

Note:

La bonne pratique est de :
  * utiliser des versions précises, plutôt que latest
  * gérer ses dépendances dans un fichier que l’on pourrait mettre en gestion de configuration

permettant ainsi de s’assurer que l’exécution sera effectuée avec une version testée des dépendances.

--

Spécifier la version avec la commande ansible-galaxy :
  * Installation d’un rôle : `ansible-galaxy role install geerlingguy.apache:2.0.0`
  * Installation d’un rôle : `ansible-galaxy role install geerlingguy.apache:2.0.0`
  * Installation d’une collection : `ansible-galaxy collection install incubateurpe.haproxy:==0.0.4`

Ansible Galaxy ne modifiera pas une version déjà installée, il faut lui ajouter le paramètre --force

Pour les collections, il est possible de définir des ranges :
  * <span>*: Dernière version, valeur par défaut</span>
  * <span>!= : Différent de la version spécifiée</span>
  * <span>== : Exactement la version spécifiée</span>
  * <span>= : Au moins la version spécifiée</span>
  * <span>> : Version supérieure à la version spécifiée</span>
  * <span><= : Jusqu’à la version spécifiée</span>
  * <span>< : Version antérieure à la version spécifiée</span>

Documentation officielle : https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#installing-an-older-version-of-a-collection

---

## Ansible Galaxy : Le fichier requirements

Les dépendances Galaxy peuvent être listées dans un fichier de requirements, contenant des rôles et des
collections avec des versions précises ou des plages de versions.

Exemple :

```yaml 
roles:
  - name: geerlingguy.java
    version: 1.9.6
collections:
  - name: incubateurpe.haproxy
    version: '>=0.0.3'
    source: https://galaxy.ansible.com
```

Documentation officielle : https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#install-multiple-collections-with-a-requirements-file

--

Un rôle peut être récupéré directement depuis son repository git.

Exemple :

```yaml
roles:
  - name: incubateurpe.kubernetes
    src: https://gitlab.com/incubateur-pe/kubernetes-bare-metal.git
    scm: git
    version: 1.0.3
  - name: geerlingguy.java
    version: 1.9.6
collections:
  - name: incubateurpe.haproxy
    version: '>=0.0.3'
    source: https://galaxy.ansible.com
```

Documentation officielle : https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#install-multiple-collections-with-a-requirements-file

--

Installer des dépendances depuis un fichier requirements

`ansible-galaxy install -r requirements.yml`

Remplacer les dépendances existantes

`ansible-galaxy install -r requirements.yml --force`

---

## Ansible Galaxy - TP2 : utiliser un fichier de requirements (~5 min.)

![TP](./images/TP.png)

💡 Scrollez vers le bas pour les détails du TP ( 1 page )

--

## TP2

🎯 L’objectif de ce tp est de contrôler les versions des ressources Galaxy utilisées dans votre projet en s’appuyant sur un fichier requirements.yml.

1. À la racine de votre projet, créer un fichier **requirements.yml**

2. Fixer la version de la ressource **geerlingguy.apache** sur la version 3.1.3

3. Fixer la version de la ressource **incubateurpe.pacemaker** sur la version 0.0.6

4. Lancer l’installation des dépendances
  `ansible-galaxy install -r requirements.yml`

5. Que constatez-vous ?

6. Relancer la commande d’installation des dépendances en forçant cette fois-ci Ansible à récupérer les versions demandées

👍 Contrôler les versions de vos dépendances permet de se prémunir d’une montée de version automatique d’une dépendance qui pourrait venir casser la compatibilité avec votre playbook et entraîner un dysfonctionnement non prévu.


---

## Ansible Galaxy : Le fichier requirements - à retenir

> Utiliser des versions fixes est une bonne pratique, elle permet de s’assurer que notre code continuera de fonctionner correctement même si une nouvelle version d’une dépendance est publiée.

> L’utilisation du fichier de requirements permet de centraliser toutes les dépendances et leurs versions, et de les installer facilement.

> Les dépendances peuvent provenir de différentes sources, comme git par exemple, mais peuvent aussi prendre la forme d’un fichier tar versionné.

---

## Ansible Galaxy : pour aller plus loin - créer un rôle

La structure d’un rôle créé avec Ansible Galaxy est la même qu’un rôle “normal”.

Avant de publier un rôle sur la plateforme de la communauté, il faut absolument :
  * un fichier meta/main.yml renseigné
  * une documentation dans le README.md

La publication sur galaxy.ansible.com nécessite que le rôle soit dans un projet github.

Documentation officielle : https://docs.ansible.com/ansible/latest/galaxy/dev_guide.html

Note:

Au sujet de la publication sur galaxy.ansible.com, ce dernier est très lié à github, et donc, même si les sources sont sur un autre serveur git, il faudra les synchroniser sur un repository github pour permettre à galaxy de les récupérer.

Cette limitation n’existe que pour les rôles et leur publication sur le repository public, les collections peuvent être hébergées sur n’importe quel repository

---

## Ansible Galaxy : aller plus loin - créer une collection

Les collections Galaxy sont un assemblage de rôles, de plugins et de modules,
et suivent la structure ci-contre.

<pre><code class="hljs" style="float: right; max-height: 600px; font-size: 0.8em; margin-right: 30px;">collection/
├── meta/
├── docs/
├── galaxy.yml
│ └── runtime.yml
├── plugins/
│ ├── modules/
│ │ └── module1.py
├── README.md
├── roles/
│ ├── role1/
│ └── .../
├── playbooks/
│ ├── files/
│ ├── vars/
│ ├── templates/
│ └── tasks/
└── tests/
</code></pre>

Tout comme les rôles, les collections ont des métadonnées qui permettent de
définir:
  * l’auteur
  * le namespace
  * les dépendances
  * …

Ces métadonnées sont essentielles et doivent être décrites dans le fichier
galaxy.yml

Une collection est également utilisable depuis un entrepôt git.

La publication d’une collection ne nécessite pas d’entrepôt GitHub, seulement
d’un compte ayant des droits sur le namespace

Documentation officielle : https://docs.ansible.com/ansible/latest/dev_guide/developing_collections.html

---

## Ansible Galaxy : objectifs atteints ?

<img align="left" src="./images/tick_guy.png" style="border: none;margin-right: 40px;">
<br/>

A l’issue de ce module, vous êtes maintenant en mesure :
  * d’utiliser des ressources de la communauté depuis vos playbooks
  * de gérer les dépendances de votre playbooks

Note:

Profiter de ce slide pour faire un point sur ce qui a été appris et prendre de la hauteur sur les choses abordées dans le module. S’assurer que les objectifs sont atteints pour tous les participant.e.s 