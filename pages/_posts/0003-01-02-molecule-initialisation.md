## Molecule - Initialisation

**Initialiser Molecule dans un rôle existant :**

Il est possible d’initialiser la configuration de Molecule après la création d’un rôle, pour cela, on crée un nouveau scénario :

```shell
molecule init scenario mon_scenario --driver-name docker
```
Si le scénario n’est pas nommé, il prendra la valeur default

Si le driver n'est pas nommé, docker sera utilisé par défaut

---

## Molecule - Arborescence

Molecule crée un dossier nommé “molecule” dans le rôle.

<pre><code class="hljs" style="float: left; max-height: 400px; font-size: 0.8em; margin-right: 30px;">molecule

└── default

├── converge.yml

├── molecule.yml

└── verify.yml
</code></pre>

Dossier de configuration molecule

Scénario “default”

Playbook de provisionning des instances

Configuration des ressources du scénario

Playbook de tests unitaires

Les scénarios correspondent donc simplement à une arborescence contenant des fichiers de configuration.

Il est possible de créer d’autres scénarios en :
  * utilisant la commande molecule init scenario
  * dupliquant le répertoire default

L’utilisation d’un scénario autre que celui nommé “default” nécessite d’ajouter le paramètre -s scenario aux commandes Molecule

---

## Molécule - Configuration des ressources

**molecule.yml :**

```yaml
---
dependency:
 name: galaxy
driver:
 name: docker
# Configuration de l'infra de test
platforms: 
 # tableau des instances
 - name: instance
   image: docker.io/pycontribs/centos:8
   # Définit si l’image contient déjà l’outillage ansible ou si elle doit être reconstruite
   pre_build_image: true
provisioner:
 name: ansible
verifier:
 name: ansible
```

---

## Molecule - Playbook de test

**converge.yml :**

Le fichier converge.yml est un playbook Ansible utilisé par Molecule pour exécuter le rôle sur les instances cibles.

```yaml
- name: Converge
  hosts: all
  tasks:
    - name: "Include mon_role"
      include_role:
        name: "mon_role"
```

Ce playbook permet donc d’appliquer l’état final attendu aux instances pour exécuter des tests.

Il est également utilisé pour le test d’idempotence, Molecule ré-exécute ce playbook pour vérifier qu’aucune modification n’est apportée.

`molecule converge` : lancer le playbook Converge pour provisionner les instances de test

---

## Molecule - Debugguer - verbosité

Mode debug de Molecule :

```shell
molecule --debug "commande"
```

Augmenter la verbosité d’Ansible :

```yaml
#molecule/default/molecule.yml:
provisioner:
 name: ansible
 options:
   vvv: True
```

---

## Molecule - Déboguer - accès aux instances

Il est possible d’accéder aux instances créées par Molecule, quel que soit le type d’instance (docker, vagrant, libvirt, etc) avec la commande molecule login

![login](./images/loginmolecule1.png)

Dans le cas ou plusieurs instances sont créées, il faut préciser l'hôte cible :

![login](./images/loginmolecule2.png)

---

## Molecule - TP1 : créer un rôle et lancer Molecule

![TP](./images/TP.png)

💡 Scrollez vers le bas pour les détails du TP ( 3 pages )

Note:

En fin de TP, démontrer l’utilisation des deux fichiers présentés précédemment : le container docker porte le nom de l’instance de la section platforms et le playbook converge a bien été exécuté

--

## TP1

🎯 L’objectif de ce tp est de créer un rôle indépendant et partageable que vous créerez et testerez avec Molecule. Ce sera testé sur un environnement éphémère créé par Molecule en utilisant Docker.

1. Depuis le terminal, se positionner dans le home directory
2. Molecule avec le driver Docker est déjà installé sur votre machine, tester son installation avec la commande suivante : `molecule --version`

Si votre installation est correcte, vous devriez obtenir :

```
molecule 6.0.2 using python 3.10
 ansible:2.15.4
 azure:23.5.0 from molecule_plugins
 containers:23.5.0 from molecule_plugins requiring collections: ansible.posix>=1.3.0
community.docker>=1.9.1 containers.podman>=1.8.1
 default:6.0.2 from molecule
 docker:23.5.0 from molecule_plugins requiring collections: community.docker>=3.0.2
ansible.posix>=1.4.0
 ec2:23.5.0 from molecule_plugins
 gce:23.5.0 from molecule_plugins requiring collections: google.cloud>=1.0.2
community.crypto>=1.8.0
 podman:23.5.0 from molecule_plugins requiring collections: containers.podman>=1.7.0
ansible.posix>=1.3.0
 vagrant:23.5.0 from molecule_plugins
```

--

3. Créer un rôle nommé **mon_role** et un scenario en utilisant la commande molecule :

```
ansible-maitre:~$ ansible-galaxy init mon_role ; cd mon_role
ansible-maitre:~$ molecule init scenario --driver-name docker
```

4. Ajouter le dossier **mon_role** à votre workspace VS Code via le menu File => Add Folder to Workspace
5. Modifier les fichiers du dossier **molecule/default** en utilisant ceux du répertoire *molecule_config* du projet git de la formation
  * Modifier le fichier **molecule/default/molecule.yml** de votre projet **mon_role** pour configurer Molecule avec une instance nommée **ma-cible-ubuntu** utilisant l’image **geerlingguy/docker-ubuntu2004-ansible**
  * Modifier le fichier **meta/main.yml** du role pour y ajouter `role_name: mon_role` et `namespace: formation` :
```yaml
galaxy_info:
  role_name: mon_role
  namespace: formation
```

💡 Dans le cadre du TP, nous choisissons de tester notre rôle sur la distribution Ubuntu 20.04, mais vous pouvez tester vos rôles sur n’importe quelle autre distribution.

geerlingguy met à disposition sur le hub Docker https://hub.docker.com/u/geerlingguy des images de différentes distributions Linux (Ubuntu, CentOS, Debian, etc.) que vous pouvez utiliser pour vos tests Molecule.

--

6. Ajouter dans le fichier **main.yml** des tasks du rôle, une task utilisant le module copy afin de copier un contenu dans un fichier.

Paramètres du module :
  *  chemin : /etc/config_formation
  *  droits : 0644
  *  owner: root
  *  group: root
  *  contenu : formation=true

7. Dans le terminal, se positionner dans votre projet mon_role et lancer la commande `molecule converge`

8. Une fois la commande converge effectuée, Molecule aura provisionné votre environnement Docker éphémère et exécuté les tâches de votre rôle sur votre instance **ma-cible-ubuntu**, instance qui est un conteneur Docker géré par Molecule.

9. Exécuter la commande suivante pour voir les instances gérées par Molecule : `molecule list`   

10. Vérifier sur l’instance **ma-cible-ubuntu** que le fichier **/etc/config_formation** a bien été créé par Ansible. Pour cela, se connecter sur l’instance via la commande `docker exec -it ma-cible-ubuntu /bin/bash` 

---

## Molecule - Initialisation - à retenir

![thinking](./images/thinking.png)

L’utilisation de Molecule peut se faire à n’importe quel moment du cycle de vie du rôle.

Il est possible de définir plusieurs scénarios correspondant à des cas d’utilisation différents.

Le playbook converge.yml est exécuté pour provisionner les instances et pour vérifier l’idempotence
