## Molecule - Séquence

<pre><code class="hljs" style="float: right; max-height: 400px; font-size: 0.8em; margin-left: 30px;">--> Test matrix
└── default
    ├── dependency
    ├── lint
    ├── cleanup
    ├── destroy
    ├── syntax
    ├── create
    ├── prepare
    ├── converge
    ├── idempotence
    ├── side_effect
    ├── verify
    ├── cleanup
    └── destroy
</code></pre>

Les commandes Molecule peuvent potentiellement appeler plusieurs actions, ce sont les séquences.

  * Les séquences disposent d’une configuration par défaut. 
  * Les séquences ne sont pas liées entre elles.

Le contenu d’une séquence peut être lu avec la commande : `molecule matrix <nom_sequence>`

Exemple avec configuration de la séquence converge :

```
$ molecule matrix converge
--> Test matrix
└── default
    ├── dependency # récupération des dépendances
    ├── create     # création de l'infra
    ├── prepare    # playbook de préparation de l'infra
    └── converge   # playbook d'exécution du role
```

---

## Molecule - Séquences disponibles

| Commande | Action | Playbook | Matrice modifiable |
|----------|--------|----------|--------------------|
| check | Exécute le rôle en dry-run| -| Oui|
| converge| Provisionne les instances| converge.yml| Oui |
| create| Crée les instances| create.yml| Oui| 
| dependency| Installe les dépendances| - | Non |
| destroy| Supprime les instances| destroy.yml| Oui| 
| idempotence| Vérifie l’idempotence du rôle| -| Non| 
| lint| Vérifie les règles d’écriture| -| Non| 
| prepare| Prépare les instances avant provisionning| prepare.yml| Non| 
| side-effect| Teste les impacts du provisionning| side_effect.yml| Non| 
| syntax| Vérifie la syntaxe du rôle| -| Non| 
| test| Chaîne de tests complète| -| Oui| 
| verify| Exécute les tests unitaires| verify.yml| Non| 

Note:

Pour le playbook verify, ce n’est pas applicable à testinfra, uniquement pour le verifier ansible

---

## Molecule - Modification des séquences

Le déroulement d’une séquence est modifiable, il se configure dans la section scenario du fichier molecule.yml

Par défaut : 
```shell
$ molecule matrix destroy
--> Test matrix
└── default
    ├── dependency
    ├── cleanup
    └── destroy
```

Configuration:

```yaml
#molecule/default/molecule.yml :
scenario:
  destroy_sequence:
    - destroy
```

Résultat:
```shell
$ molecule matrix destroy
--> Test matrix
└── default
    └── destroy
```

---

## Molecule : Customiser le playbook

Certaines actions utilisent donc des playbooks Ansible pour s’exécuter.

Il est possible de les customiser pour, par exemple, configurer les instances et préparer l’exécution du rôle, ou encore tester une exécution en plusieurs temps.

Certaines actions disposent d’un playbook par défaut (create / destroy) et sont surchargeables.

Exemple avec la séquence prepare :

```yaml
#molecule/default/prepare.yml :
---
- name: Prepare
  hosts: all
  gather_facts: false
  tasks:
    - name: Installe les pré-requis python pour k8s
      pip:
        executable: /usr/bin/pip3
        name:
         - openshift>=0.6
         - PyYAML>=3.11
         - jmespath
```

Note:

Exemple d’exécution en plusieurs temps : le rôle installe d’abord des masters puis des slaves

---

## Molecule - Quel playbook modifier?


<img align="left" width="400" height="400" src="./images/question.png" style="border: none;margin-right: 20px;">
<br/>

Lors du TP précédent, nous avons modifié le playbook converge.yml pour créer un utilisateur.

  * Ce playbook est-il le meilleur endroit pour ce type d’actions ?
  * Comment pourrions-nous faire mieux?

Nous avons déjà customisé une séquence, laquelle?

Note:

Ce n’est pas le meilleur endroit, en effet, dans le converge, nous souhaitons appliquer uniquement ce qui est lié à notre rôle testé, pas à ses dépendances. En d’autres termes, nous ne voulons pas tester une dépendance déjà testée par ailleurs…

Pour faire mieux, il faudrait pouvoir déplacer ces actions dans la séquence prepare, qui est faite pour cela

La séquence customisée est “verify”, nous avons redéfini son comportement avec le playbook verify.yml

---

## Molecule - TP4: Customiser les séquences

![TP](./images/TP.png)

💡 Scrollez vers le bas pour les détails du TP ( 1 page )

--

## TP4

🎯 L’objectif de ce TP est de configurer molecule pour qu’il n'exécute que le nécessaire lors des tests, notamment d’idempotence. Nous allons pour cela modifier un playbook, et configurer le conportement d’une séquence

Dans le fichier molecule/default/prepare.yml :
* Récupérer la création de l’utilisateur depuis le fichier converge.yml

Dans le fichier molecule/default/converge.yml :
* Supprimer la création de l’utilisateur

Dans le fichier molecule/default/molecule.yml:
* Créer la section :

```yaml
scenario:
  destroy_sequence:
    - destroy
```

* Exécuter l’intégralité des tests : `molecule test`  

💡  Le playbook prepare.yaml est le plus à même de porter la création de l’utilisateur, en effet, nôtre rôle d’exemple effectue de la configuration, il pourrait s’attendre à trouver un utilisateur déjà présent. Nous avons simulé ce comportement dans ce TP.

---

## Molecule - Séquences et dépendances - à retenir

![thinking](./images/thinking.png)

Le requirements.yml d’un rôle peut être lu par Molecule pour préparer l’environnement local.

Le comportement par défaut de Molecule est configurable.

Certaines actions utilisent des playbooks par défaut qui peuvent être customisés selon nos besoins.