## Ansible : les rôles - à quoi sert un rôle ?

![roles](./images/role1.png)

Un rôle est une structure de dossier formalisée définissant un ensemble de tâches, de variables, de fichiers qui forment un ensemble logique et réutilisable.

Les rôles permettent de facilement organiser, réutiliser et partager le code des playbooks.

**Ansible Galaxy** est une communauté sur laquelle il existe des milliers de rôles partagés.

Note:

Tous les rôles n’ont pas vocation à être partagés ou réutilisés, il permettent avant tout d’organiser et de segmenter son code, d’en faciliter la maintenance.

Pour faire simple, on peut comparer les rôles Ansible aux librairies dans les langages de programmation. 

---

## Ansible : les rôles - une arborescence normalisée


<pre><code class="hljs" style="float: left; max-height: 400px; font-size: 0.8em; margin-right: 30px;">mon-projet-ansible
  playbook.yml
  roles
    role1
      tasks
      defaults
      vars
      files
      templates
      handlers
      meta
      README.yml
    role2  
</code></pre>

Arborescence des répertoires d’un rôle :

  * tasks : les tâches associées au rôle. Le rôle se base sur le fichier main.yml pour exécuter les tâches.
  * defaults : les variables par défaut associées au rôle.
  * vars : les autres variables associées au rôle.
  * files : des fichiers à copier sur les serveurs cibles.
  * templates : des fichiers variabilisés à copier sur les serveurs cibles.
  * handlers : les actions à déclencher après l’exécutions des tâches.
  * meta : les métadonnées du rôle (auteur, dépendances à d’autres rôles, etc.)


Par défaut, Ansible cherchera un fichier main.yml dans chacun des répertoires à l’exception de files et templates. 

D’autres fichiers peuvent être ajoutés et référencés dans le fichier main.yml du répertoire associé en utilisant la directive include.

```yaml
#exemple main.yml dans tasks
- include_tasks: "mon_fichier_tasks1.yml"
- include_tasks: "mon_fichier_tasks2.yml"
```

Note:

Les vars du rôle ont une précédence élevée, permettant de définir des variables qui ne seront pas écrasées malencontreusement par une variable du play.

Préciser que nous verrons par la suite comment créer, si besoin, automatiquement cette structure

---

## Ansible : les rôles - comment les utiliser ?

Les rôles ne peuvent être appelés que depuis un playbook en utilisant le paramètre **roles** sur un play. Ce paramètre prend en entrée une liste de rôles qui seront exécutés dans l’ordre de déclaration.

Exemple d’utilisation dans un playbook :

```yaml
- hosts: all
  roles:
    - role1
    - role2 
```

* par défaut, Ansible va chercher les rôles référencés dans les playbooks dans les 2 répertoires suivants :
  * répertoire roles du projet
  * répertoire /etc/ansible/roles 
* option *roles_path* (ansible.cfg) : définir un nouveau path

Note:

Les rôles ne peuvent être appelés que depuis un playbook. Le paramètre rôles du play prends en entrée un tableau, les rôles seront exécutés dans l’ordre de déclaration

---

## Ansible : les rôles - pre et post tasks


<pre><code class="lang-yaml hljs" style="float: left; max-height: 400px; font-size: 0.8em; margin-right: 30px;">- name: **nom du play**
  hosts: ** hosts **
  pre_tasks:
    - name: ** nom task **
      copy:         
        src: ** fichier source **
        dest: ** destination **
  roles :
    - role1
  post_tasks:
    - name: ** nom task **
      template:         
        src: ** fichier source **
        dest: ** destination **
</code></pre>

<br style="margin-top: 5px;"/>

Dans un play, il est possible de définir des tasks et des rôles, pour ordonnancer l’exécution, il existe les directives :

  * Les pre_tasks seront exécutées avant les rôles
  * Les post_tasks seront exécutées après les rôles

Les tasks sans préfixe seront considérées comme des post_tasks.

---

## Ansible - TP6 : créer son premier rôle


![TP](./images/TP.png)

💡 Scrollez vers le bas pour les détails du TP ( 2 pages )

Note:

Ce TP est a exécuter itérativement, et tester l’accès au serveur web, à chaque itération, les stagiaires vont rencontrer des problèmes liés à l’activation du service

--

## TP6.1

🎯 L’objectif de ce TP est de créer un rôle nommé apache qui va permettre d’installer un serveur web apache sur vos machines cibles.

  1. À la racine de votre projet, créer un répertoire nommé *roles* dans lequel vous créerez un répertoire nommé *apache*.
  1. Créer dans le répertoire *apache*, un répertoire nommé *tasks* qui contiendra un fichier nommé *main.yml*
  1. Editer le fichier *main.yml* pour y ajouter les tasks suivantes :
  * une task permettant d’installer la dernière version du package “apache2” sur une machine Debian
  * une task permettant de s’assurer pour une machine Debian que le service "apache2" est bien démarré
  * une task permettant d’installer la dernière version du service “httpd” sur une machine AlmaLinux
  * une task permettant de s’assurer pour une machine AlmaLinux que le service “httpd” est bien démarré

  💡 Pensez à utiliser le module [package](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/package_module.html) et le module [service](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/service_module.html).

--

## TP6.2

  1. À la racine de votre projet, créer un nouveau playbook nommé install-apache.yml avec un play ciblant tous les hôtes et appelant le rôle apache que vous venez de créer.
  1. Exécuter votre playbook install-apache.yml
  1. Pour vérifier que vos serveurs web Apache sont tous opérationnels, vous pouvez lancer Firefox et accéder aux pages web de test de vos serveurs web Apache en saisissant l’adresse de vos machines cibles dans la barre d’adresse : `<numero>-<distribution>-<numero-cible>.formationansible.sii24.pole-emploi.intra`

  💡 Vous avez créé manuellement l’arborescence de votre rôle (répertoires avec fichiers
main.yml), mais nous aurions pu la créer avec la commande :
`ansible-galaxy role init apache`