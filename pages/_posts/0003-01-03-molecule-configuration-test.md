## Molécule - Configuration des tests

La configuration des tests est dépendante du **verifier** utilisé :

Dans le cas du verifier Ansible, Molecule utilise le fichier **verify.yml** dans l’arborescence du scénario. 

Ce fichier est un playbook qui contient une liste de tasks. 

Exemple:

```yaml
- name: Verify
  hosts: all
  tasks:
  - name: Récupère les informations du fichier
    stat:
      path: /etc/motd
    register: stats_motd
  - name: Confirme l’existence du fichier
    assert:
      that: stats_motd.stat.exists
```

`molecule verify` : lancer les tests

`molecule test` : lancer la création des instances, exécuter les tests et supprimer les instances

Note:

Évoquer ici le verifier testinfra et ce qu’il permet.

---

## Molecule - TP2 : réaliser vos premiers tests unitaires

![TP](./images/TP.png)

💡 Scrollez vers le bas pour les détails du TP ( 2 pages )

Note:

En fin de TP, démontrer l’utilisation des deux fichiers présentés précédemment : le container docker porte le nom de l’instance de la section platforms et le playbook converge a bien été exécuté

--

## TP2

🎯 L’objectif de ce TP est de réaliser des tests unitaires sur votre rôle mon_role afin de vous assurer de manière automatique que le résultat des tasks soit conforme à ce que vous attendez

Dans le fichier **molecule/default/verify.yml** :

1. Un play nommé “Vérifier la création du fichier config_formation” ayant pour cible votre instance *ma-cible-ubuntu*
1. Ajouter dans votre play, une task utilisant le module [stat](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/stat_module.html) pour récupérer dans une variable des informations sur le fichier */etc/config_formation* présent sur votre instance *ma-cible-ubuntu*
1. Ajouter une task utilisant le module [assert](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/assert_module.html) pour vérifier que :
  *  le fichier existe
  *  le fichier appartient bien au groupe root
  *  le fichier appartient bien au user root
  *  les droits du fichier sont 0644
4. Pour exécuter le playbook verify.yml qui va lancer vos tests unitaires, exécuter la commande molecule `molecule verify`
5. Est-ce que tous vos tests passent avec succès ? Si ce n’est pas le cas, modifiez les tasks et/ou les tests de votre rôle pour terminer en succès.

--

Nous allons maintenant ajouter des tasks pour contrôler le contenu du fichier :

1. Ajouter une task utilisant le module [slurp](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/slurp_module.html) pour récupérer dans une variable le contenu du fichier */etc/config_formation*
1. Ajouter une task utilisant le module assert pour vérifier que le fichier contient bien “formation=true”.
1. Lancer de nouveau vos tests unitaires et vérifier qu’ils se terminent avec succès.

👍 Félicitations ! Vous avez testé de manière automatisé et autonome votre rôle. Vous pourriez partager ce rôle avec votre équipe en le publiant sur un repository git (ex : GitLab Pôle emploi), ou avec la communauté en le publiant sur un repository GitHub et en le référençant sur Ansible Galaxy.

💡 Par la suite, vous pourriez facilement intégrer le lancement de vos tests unitaires et de l’analyse du contrôle de la qualité syntaxique dans un pipeline CI/CD afin de vous assurer de la qualité des évolutions apportées à votre rôle, de vous assurer que votre rôle continu à remplir correctement sa fonction et d’éviter des risques de régression.

---

## Molecule - tests unitaires - à retenir.

![thinking](./images/thinking.png)

Provisionner les containers de test nous assure que :
  * le rôle s'exécute sans erreur
  * le rôle est idempotent

Ce n’est pas suffisant, seuls les tests unitaires confirment que les actions effectuées par Ansible correspondent à ce que l’on souhaitait.

Les tests peuvent être décrits avec Ansible dans le fichier **verify.yml**