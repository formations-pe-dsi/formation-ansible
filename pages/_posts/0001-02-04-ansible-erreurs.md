## Ansible : gestion des erreurs

<img align="left" width="400" height="400" src="./images/erreurs1.png" style="border: none;margin-right: 20px;">

**Comportement par défaut** : 

Si une erreur arrive dans l’exécution des tâches sur un serveur, Ansible sort ce serveur de l’exécution du play et continue à exécuter les tâches suivantes sur les autres serveurs.

Note:

Utile si l’on fait de la gestion de parc avec ansible, un host ayant un comportement inadéquat sera simplement retiré du play, laissant la mise à jour se faire sur les autres

---

## Ansible : gestion des erreurs - 2

<img align="left" width="400" height="400" src="./images/erreurs2.png" style="border: none;margin-right: 20px;">

En cas d’erreur, il est possible de vouloir garder une certaine consistance en stoppant l'exécution d’un play pour tous les hôtes.

Pour cela, il est possible d’utiliser l’option `any_errors_fatal: true` dans un play ou sur une tache.

---

## Ansible : gestion des erreurs - 3

Voici quelques options possibles pouvant être positionnées sur une tâche :

* `ignore_errors` pour ignorer les erreurs levées
* `ignore_unreachable` pour ignorer une erreur qui serait dûe à un hôte injoignable
* `failed_when` pour conditionner la levée d’une erreur 

```yaml
tasks:
- name: Tâche qui échoue si taille disponible dans /tmp < 1Go
    shell: "df -k /tmp|grep -v Filesystem|awk '{print $4}'|tail -1"
    register: tmpspace
    failed_when: "tmpspace.stdout|float < (1024 * 1024)"
```

* `changed_when` pour annuler le résultat “changed” d’une tâche. Cette condition permet notamment dans certains cas de rendre idempotent l’exécution d’une tâche.

---

## Ansible : gestion des erreurs - les blocs

Il est possible d’utiliser dans un play les sections *block*, *rescue* et *always* pour regrouper logiquement des tâches et la gestion des erreurs.

```yaml
tasks:
  - name: Gestion par blocs
    block:
      - debug:
          msg: "Exécution normale"
    rescue:
      - debug:
          msg: "Si une erreur est levée, que dois-je faire ?"
    always:
      - debug:
          msg: "Je suis toujours exécutée"
```

---

## Ansible - TP5 : gérer les erreurs


![TP](./images/TP.png)

💡 Scrollez vers le bas pour les détails du TP ( 2 pages )

--

## TP5.1

🎯  L’objectif de ce TP est de créer un playbook permettant de comprendre le comportement d’Ansible sur la gestion des erreurs.

1. Dans votre playbook `init-configuration.yml`, ajouter après la task de création du fichier "alma_flag", une task qui exécutera un cat du fichier ”alma_flag” en utilisant le module `command`. Task sans condition pour qu’elle s’exécute sur toutes les machines cibles.
2. Exécuter le playbook et constater que sur `ansible-cible-2` et `ansible-cible-3`, toutes les tasks ont bien été exécutées malgré l’erreur sur la machine `ansible-cible-1`.
3. Modifier le play pour que l’exécution s'arrête pour toutes les machines cibles si une erreur se produit.
4. Exécuter à nouveau le playbook. L’exécution de votre play a normalement été stoppée après l’exécution de la task cat en erreur mais Ansible a tout de même terminé l'exécution de cette task cat sur tous les hôtes malgré l'erreur, ansible s'assure donc de l'état de tous les hôtes avant d'arreter l'exécution.

--

## TP5.2

5. Modifier la task exécutant le cat pour qu’elle n’échoue que si le code retour de la commande est supérieur à 1.

    💡 Il vous sera nécessaire de capturer la sortie de la commande cat dans une variable pour récupérer son code retour (code retour = paramètre nommé rc dans le retour de la commande).

6. Exécuter à nouveau le playbook. L’exécution devrait se dérouler sans erreur.
7. Exécuter à nouveau le playbook pour constater que la task qui effectue un cat sur le fichier “flag” n’est pas idempotent.
8. Modifier cette task pour qu’elle n’affiche un changement que si la stdout n’est pas vide.

---

## Ansible - TP5 : debrief

![thinking](./images/thinking.png)

Dans le TP, nous avons utilisé un module non **idempotent** : le module *command*. Ce module ne peut pas détecter si la commande appelée a effectué une modification sur la cible, il faut donc gérer l’idempotence nous même.

Plusieurs modules sont dans ce cas, en voici quelques exemples :
  * command
  * shell
  * url (module d’appel de webservices)

En utilisant le paramètre *register* et la directive *changed_when*, il est possible de détecter les changements, et d’effectuer un retour d’information correct afin d’obtenir un comportement idempotent.