## Ansible : playbook - structuration du fichier

Les fichiers playbook, au format YAML, contiennent des groupements d’instructions (play) qu’Ansible pourra exécuter sur les machines cibles. Ils permettent de définir des scénarios complexes mais de manière simple.

Une instruction (task) peut être :
  * l’exécution une commande
  * l’exécution d’un script
  * l’installation d’un package
  * etc...

Ces instructions sont exécutées par l’intermédiaire de modules.

  * Exécuter un playbook : `ansible-playbook **nom du fichier playbook**`
  * Vérifier la syntaxe d’ un playbook : `ansible-playbook --syntax-check **nom du fichier playbook**`
  * Vérifier les règles d'écriture : `ansible-lint **nom du fichier playbook**`

Note:

Les playbooks sont la solution pour créer des scénarios d’orchestration complexes.

Un playbook peut contenir N plays, chacun des plays est indépendant et le contexte ( variables, facts etc… ) n’est pas conservé entre deux plays différents.

Les playbooks étant écrits en YAML, l’alignement des lignes et des mots-clés est crucial. 

--

## Exemple de playbook

```yaml
#structure d’un playbook
- name: nom du play
  hosts: cible1, cible2
  tasks:
   - name: nom de la task
     nom_module:
       parametre: valeur
       parametre: valeur
   - name: nom de la task
     nom_module:
       parametre: valeur
       parametre: valeur
```

---

## Ansible : playbook - Exécution parallèle

<img align="left" width="250" height="300" style="margin-right: 10px;" src="./images/exec_parallele.png">

L’exécution d’un play se fait sur un ensemble de hosts. 

Chacune des tasks sera exécutée par défaut en parallèle sur tous les hosts du play.

Une fois l’exécution terminée sur tous les hosts, la task suivante est lancée.

Par défaut, Ansible utilise 5 processus simultanés (forks).  
Cette configuration par défaut est modifiable:
```ini
#fichier ansible.cfg
[defaults]
forks = 10
```

Note:

Il est possible de contrôler le comportement d’ansible pour limiter l’exécution parallèle, ou même exécuter host par host, ces paramètres sont hors scope de ce module de formation.

Le nombre de communication simultanées peut être contrôlé dans le fichier de configuration dans la section défaults

---

## Ansible : playbook - cibler l’exécution

Le paramètre `hosts` d’un play permet de limiter l’exécution à un ensemble de cibles en utilisant les patterns suivants :

| Description | Pattern | Cibles |
|-------------|---------|--------|
| Tout l’inventaire | all (ou *) | |
| Une machine | host1 | |
| Plusieurs machines | host1:host2 (ou host1,host2) | |
| Un groupe | webservers | |
| Plusieurs groupes | webservers:dbservers | Tous les hosts de  webservers et de dbservers |
| Exclure un groupe | webservers:!paris | Tous les hosts de webservers sauf ceux de paris|
| Intersection de groupes | webservers:&staging | Les hosts de webservers présents dans staging |
| Une machine et un groupe | host1,dbservers | Tous les hosts de dbserver et host1 |

---

## Ansible : modules


<pre><code class="lang-yaml hljs" style="float: right; max-height: 400px;">#fichier playbook
- name: Ajout de l’utilisateur
  hosts: all
  tasks:
   - name: Ajouter le groupe "formation"
     ansible.builtin.group: # Utilisation du module "group"
       name: formation
       state: present
   - name: Ajouter l'utilisateur "formation"
     ansible.builtin.user: # Utilisation du module "user"
       name: formation
       comment: Utilisateur de formation ansible
       uid: 2500
       group: formation
</code></pre>

Les modules sont des scripts Ansible réutilisables qui permettent d’interagir avec votre machine, une machine distante, une API, etc…

Ils disposent :
  * d’une interface
  * de paramètres
  * d’une structure de retour


Un module peut être appelé depuis la ligne de commande ou depuis une task dans un playbook

Note:

Décrire la structure de la task : 
  * name: nom de l’action, sera logguée sur la stdout a l’exécution
  * nom du module: module à utiliser
  * paramètres du module : description de l’état que le module doit atteindre
  * paramètres de la tâche: permettent de contrôler l’exécution ( indentation au même niveau que le name )

Le tiret avant name fait partie du format yaml ( on crée un tableau donc on met un tiret)

Nous les avons déjà utilisés, notamment avec le ping effectué précédemment.

---

## Ansible : une multitude de modules

  * Une quantité de modules importante, il existe quasiment toujours un module pour l’action que l’on souhaite faire.
  * Une documentation très bien faite et très complète https://docs.ansible.com/ansible/latest/collections/ansible/builtin/index.html
  * Documentation offline : `ansible-doc <nom_module>`

**Quelques exemples de modules**:
  * [ansible.builtin.package](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/package_module.html#ansible-collections-ansible-builtin-package-module) : gestionnaire de packages générique
  * [ansible.builtin.service](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/service_module.html#ansible-collections-ansible-builtin-service-module) : contrôler services d’un hôte
  * [ansible.builtin.file](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html#ansible-collections-ansible-builtin-file-module) :  gestion des fichiers/répertoires/liens et de leurs propriétés
  * [ansible.builtin.copy](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/copy_module.html#ansible-collections-ansible-builtin-copy-module) : copier des fichiers sur un hôte
  * [ansible.builtin.lineinfile](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/lineinfile_module.html#ansible-collections-ansible-builtin-lineinfile-module) : gestion des lignes d’un fichier


Note:

Importance de la documentation ansible : une quantité de modules importantes, impossible de se souvenir de tout, il existe quasiment toujours un module pour l’action qu’on souhaite faire.

Rechercher sur internet le module file et montrer la structure de la documentation

Beaucoup de contribution dans ansible galaxy, du coup coté ansible ils ont été dépassé par le nombre de contribution, ils sont en train de faire le point pour mieux restructurer le contenu d’ansible

---

## Ansible : idempotence

![idempotence](./images/idempotence.png)

L’idempotence lors de l'exécution, une notion centrale dans Ansible et les modules :

  * On décrit dans un playbook un état attendu, et non les actions pour atteindre cet état.
  * Un playbook doit pouvoir être exécuté plusieurs fois sans impact sur les cibles si ces dernières sont déjà dans l’état attendu
  * Les modules n’effectuent des actions sur les cibles que lorsque c’est nécessaire, et retournent des informations sur ce qui a été modifié.

⚠ Tous les modules ne sont pas idempotents. Nous verrons dans les prochains chapitres comment gérer l’idempotence

Note:

Une opération est  idempotente si elle donne exactement le même résultat que vous l’exécutiez une fois ou de manière répétée, sans aucune intervention. 

Cela permet de relancer le même playbook sur tous les hosts même si seulement quelques serveurs étaient en erreur auparavant. Les actions ne se feront que sur les serveurs où toutes les actions n’avaient pas été faite.

---

## Ansible - TP2 : créer son premier playbook

![TP](./images/TP.png)

💡 Scrollez vers le bas pour les détails du TP ( 2 pages )

Note:

Le constat attendu par les stagiaires est que l’exécution sur une machine déjà configurée n’a entraînée aucune modification. Il est donc possible de ré-exécuter ce playbook autant de fois que souhaité, sans aucun impact. Il est idempotent.

A souligner, comme nous décrivons un état attendu, ansible ne se chargera pas d’effectuer la remise à zéro de modification effectuée préalablement, par exemple, si un playbook crée un utilisateur nommé “user”, qu’on l’exécute, puis qu’on modifie dans le playbook pour que l’utilisateur se nomme “user2”, nous aurons au final deux utilisateurs : “user” et “user2”.

--

## TP2.1

🎯  Durant les TP, vous allez créer plusieurs playbooks qui vous permettront d’appréhender les différents concepts Ansible. Votre premier playbook créé dans ce TP aura pour but de lancer des tasks permettant de créer des groupes et des users sur vos machines cibles **ansible-cible1**, **ansible-cible2**  et **ansible-cible3**.

  1. Créer à la racine de votre projet, un playbook `utilisateurs.yml` contenant un play nommé "Création des utilisateurs" ayant pour cible tous les hôtes de votre inventaire. 
  
  2. Ajouter à ce play, les tâches suivantes : 

  * Création d’un groupe formation ayant pour gid 2005 
  * Création d’un groupe admin ayant pour gid 500 
  * Création d’un utilisateur formation avec : 
    * uid valant 2005 
    * formation comme groupe principal 
    * admin comme groupe secondaire 
    * /bin/bash comme shell 
    * “utilisateur de formation” en commentaire

💡 Pensez à utiliser les [modules group et user](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/index.html#modules)

--

## TP2.2

3. Exécuter le playbook, vous devriez avoir le récapitulatif suivant en fin d'exécution : 

    ```
    PLAY RECAP  
    ************************************************************************************************************************************ 
    ansible-cible-1            : ok=4    changed=3    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0    
    ansible-cible-2            : ok=4    changed=3    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0    
    ansible-cible-3            : ok=4    changed=3    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
    ```

4. Exécuter à nouveau le playbook, que constatez-vous?
