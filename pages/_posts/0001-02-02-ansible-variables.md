
## Ansible : les variables

  * Ansible utilise des variables pour contextualiser ses actions
  * Plus de 20 manières pour définir des variables

Quelques exemples de variables à connaître : 

`extra vars` : variables passées en ligne de commande

`inventory vars` : variables contenues dans un inventaire

`host_vars` : variables définies dans un fichier et appliquées à un host de l’inventaire

`group_vars` : variables définies dans un fichier et appliquées à un groupe de l’inventaire

`playbook vars` : variables d’un playbook

`task vars` : variables définies dans une task

`set_facts` : variables créées dynamiquement dans une task

Note:

Pour le moment, nous savons comment décrire une suite d’actions pour atteindre un état attendu.

Cependant, c’est assez limité, nous aurons certainement besoin de conditionner certaines exécutions selon un contexte, ou encore de récupérer des informations sur le système distant ou sur l'exécution d’une tâche ( par exemple: redimensionner un FS s’il existe, ouvrir un port dans le firewall si nous sommes dans un réseau particulier )

---

## Ansible : les variables - syntaxes valides


| <span style="color: green;">Noms de variables valides</span> | <span style="color: red;">Noms de variables non valides</span> |
|---------------------------|-------------------------------|
| foo |  <span style="color: red;">*foo</span>, [Python keywords](https://docs.python.org/3/reference/lexical_analysis.html#keywords) such as <span style="color: red;">async</span> and <span style="color: red;">lambda</span> |
| foo_env | [playbook keywords](https://docs.ansible.com/ansible/latest/reference_appendices/playbooks_keywords.html#playbook-keywords) such as <span style="color: red;">environment</span> |
| foot_port | <span style="color: red;">foo-port, foo port, foo.port</span> |
| foo5, _foo | <span style="color: red;">5foo, 12<span style="color: red;"> |

---

## Ansible : les variables - structure

Les variables peuvent être définies directement dans l’inventaire, mais aussi dans le projet contenant le playbook.

<pre><code class="hljs" style="float: left; max-height: 400px; font-size: 0.8em; margin-right: 30px;">projet
├── ansible.cfg
├── playbook.yml
├── group_vars
│   ├── all
│   └── serveurs_linux
├── hosts
└── host_vars
    ├── cible-1
    └── cible-2
</code></pre>

➡ Le dossier group_vars :
  * les fichiers portent le nom des groupes, sans extension
  * le fichier all permet de définir des variables pour tous les groupes ( et donc tous les hosts )

➡ Le dossier host_vars:
  * Les fichiers portent le nom des hosts, tels que définis dans l’inventaire

Note:

Si l’on dispose de plusieurs inventaires, ces dossiers, notamment le group_vars, permettent de définir des variables liées à des groupes, utilisables dans tous les inventaires

---

## Ansible : les variables - exemple dans un playbook

{% raw %}
```yaml
- name: Affichage variables pour tous les hôtes
  hosts: all
  vars:
    regions:
     - bretagne
     - normandie
     - occitanie
  tasks:
   - name: Affichage variable
     debug:
       msg: "Ils ont des chapeaux ronds, vive la {{ regions[0] }}"
```

{% endraw %}
---

## Ansible : les variables - syntaxe - quelques exemples


{% raw %}
<pre><code class="lang-yaml hljs" style="float: left; max-height: 400px; font-size: 0.8em;">
regions:
 - bretagne
 - normandie
 - occitanie

region: "{{ regions[0] }}"

regions:
 region1: bretagne
 region2: normandie

region: regions['region1']
# Equivalent
region: regions.region1
</code></pre>
{% endraw %}

<p>
<br style="margin-top: 30px;"/>
➡ variable sous forme d’une liste
<br style="margin-bottom: 65px;"/>
➡ référencer un élément d’une liste
<br style="margin-bottom: 65px;"/>
➡ variable sous forme d’un dictionnaire
<br/><br style="margin-top: 15px;"/>
➡ référencer un élément d’un dictionnaire
</p>

Note:

Il est tout à fait possible de créer des structures complexes, une valeur d’une entrée d’un dictionnaire peut être une liste par exemple.

type simple booléen, valeurs possibles : yes / no, true / false 

Deux notations pour les éléments de dictionnaires, soit avec le “.” comme démontré dans le slide, soit avec un string entre crochets. Cela permet de référencer une entrée d’un dictionnaire à partir d’une autre variable

Quand mettre entre guillemets la valeur d’une variable: si elle contient une référence tel que : {{ foo }}.

---

## Ansible : les variables - hiérarchie des priorités et portée

<img align="right" width="700" height="300" style="margin-right: 10px;" src="./images/priorités.png">

La hiérarchie des variables est un concept de base qu’il faut connaître

À titre d’exemple, une même variable déclarée dans l’inventaire et dans un playbook prendra la valeur définie dans le playbook.

La portée d’une variable dépend de son type, soit de son positionnement dans votre projet Ansible.

Exemple :
  * une variable de type inventory host_vars n’aura comme portée que l’hôte ciblé
  * une variable de type inventory group_vars n’aura comme portée que les hôtes du groupe ciblé

---

## Ansible : les variables spéciales - les facts

  * **facts**, variables qui sont automatiquement découvertes par Ansible à partir d’un hôte géré
  * variables contenant des informations sur le système d’exploitation, les adresses IP, la mémoire, le disque, etc.

Possibilité de désactiver les facts avec l'option gather_facts :

```yaml
- name: Afficher les OS des hôtes
  hosts: all
  gather_facts: false
  tasks:
  [...]
```

Ou de les récupérer en filtrant :

```yaml
- name: Filtrer sur le fact ansible_distribution
  hosts: all
  gather_facts: false
  tasks:
    - setup:
        filter: ansible_distribution
    - debug:
        msg: "{{ ansible_distribution }}"
```

Note:

La liste des facts est très longue, beaucoup de paramètres sont récupérés. La désactivation des facts peut améliorer la performance dans certains cas.

---

## Ansible : les variables spéciales - les facts 2


`ansible  all  -m setup` : Afficher les facts recueillis en utilisant le module setup - exemple pour récupérer les facts de tous les hôtes

`ansible  all  -m setup -a 'filter=ansible_os_family'` : Affiche les facts en filtrant sur ansible_os_family

---

## Ansible : les variables spéciales - magical variables

Ansible fournit des variables spéciales dites magiques permettant de récupérer différentes informations.

Voici quelques exemples :

  * **hostvars**:<br/> Un dictionnaire contenant tous les hôtes de l’inventaire avec toutes les variables qui leur sont attribuées, y compris les facts.
  * **groups**:<br/> Un dictionnaire contenant tous les groupes de l’inventaire avec la liste des hôtes qui leur sont associés.
  * **ansible_play_hosts**:<br/> Liste des hôtes du play courant

Retrouver toutes les variables magiques disponibles sur :
https://docs.ansible.com/ansible/latest/reference_appendices/special_variables.html#magic-variables

---

## Ansible : les variables spéciales - module set_fact

  * **ansible.builtin.set_fact**, module permettant de créer à la volée des variables
  * la priorité d’un fact créé est élevée

Exemple d'utilisation:

{%raw%}
```yaml
- hosts: all
  tasks:
  - name: Récupérer la liste des interfaces de l’hôte
    ansible.builtin.set_fact:
      liste_interfaces: "{{ ansible_interfaces | select('match', '^(eth|wlan)[0-9]+') | list }}"
  - name: Afficher la liste des interfaces de l’hôte
    ansible.builtin.debug:
      msg: "Liste des interfaces de {{ ansible_hostname }} : {{ liste_interfaces }}"
```
{%endraw%}
---

## Ansible : les lookup plugins pour valoriser un fact

➡ **lookup plugins**: plugins Ansible permettant de récupérer des informations dans des sources externes (des fichiers, des APIs REST, des bases de données, etc.).

➡ Les lookups prennent des paramètres et peuvent retourner des dictionnaires, des listes ou des valeurs simples.

Quelques exemples :

  * Récupérer le contenu d’un fichier dans une variable: `{% raw %}- set_fact: contenu="{{ lookup('file', 'nom_du_fichier') }}"{% endraw %}`
  * Générer un mot de passe aléatoire: `{% raw %}- set_fact: password="{{ lookup('password', '/dev/null length=8') }}"{% endraw %}`
  * Récupérer un KV dans Vault: `{% raw %}- set_fact: kv="{{ lookup('hashi_vault', 'secret=kv_path') }}"{% endraw %}`

Retrouver la liste des lookup plugins sur https://docs.ansible.com/ansible/latest//collections/index_lookup.html

---

## Ansible : les variables spéciales - module register

➡ **register**: permet de capturer la sortie d’une task dans une variable pour une réutilisation

Exemple d’utilisation :

{%raw%}
```yaml
- hosts: all
  tasks:
   - name: ping hosts
     ping:
     register: test

   - name: afficher résultats des ping
     debug:
       msg: "{{ test.ping }}"
```
{%endraw%}
---

## Ansible - TP3 : manipuler les variables

![TP](./images/TP.png)

💡 Scrollez vers le bas pour les détails du TP ( 4 pages )

--

## TP3.1 Externaliser les variables de l’inventaire dans des fichiers 

🎯  L’objectif de ces TP est de vous faire manipuler différents types de variables afin de vous aider à assimiler le concepts de hiérarchie des priorités et de portée des variables. Dans ce but, les valeurs des variables de ce TP prendront le nom du type de la variable manipulée. 

💡 **Best Practice** : placer les variables dans des fichiers group_vars et/ou host_vars

  1. Pour ne déclarer qu’une seule fois les variables d’inventaire ansible_user et ansible_password, déplacer ces dernières dans un fichier de type group_vars ayant comme portée tous les hôtes de votre inventaire. 
  2. Rejouer le playbook utilisateurs.yml pour vérifier son bon fonctionnement après l’externalisation de la variable ansible_user.

--

## TP3.2 Afficher les facts de vos hôtes

  1. Afficher les facts de la machine ansible-cible-1 en utilisant la commande ansible et le module setup 
  2. Utiliser un filtre sur la commande précédente pour n’afficher que les informations sur la distribution

--

## TP.3.3 : Comprendre la hiérarchie des priorités et la portée des variables

1. À la racine de votre projet, créer un nouveau playbook debug-variables.yml avec un play nommé "Manipuler les variables" ayant pour cible tous les hôtes. 

2. Créer une task "Affichage variable" utilisant le [module debug](https://docs.ansible.com/ansible/2.9/modules/debug_module.html#debug-module) pour afficher le message suivant référençant une variable nommée var_test : `Hello, je suis la variable de type {% raw %}{{ var_test }}{% endraw %}`

3. Créer une variable var_test avec pour valeur “inventory group var” dans un fichier de type group_vars ayant comme portée tous les hôtes du groupe groupe1 

4. Lancer le playbook debug-variables.yml, que constatez-vous ? Notamment dans l’exécution de la task “TASK [Affichage variable]” ? 

5. Créer dans votre fichier inventaire hosts.yml une variable var_test ayant pour valeur “inventory file var” sur l’hôte ansible-cible-1 

6. Rejouer le playbook debug-variables.yml, que constatez-vous ?  

7. Créer une variable var_test ayant pour valeur “inventory host var” dans un fichier de type host_vars ayant comme portée l’hôte ansible-cible-1. 

8. Rejouer le playbook debug-variables.yml, que constatez-vous ? 

--

## TP.3.4 :

9. Dans le play “Manipuler les variables”, ajouter avant la task "Affichage variable", une task "Création d’un fact" permettant de créer une variable à la volée nommée var_test avec pour valeur "set_fact var"

10. Rejouer le playbook debug-variables.yml, que constatez-vous ? 

11. Rejouer une dernière fois le playbook debug-variables.yml en ajoutant en paramètre de la commande, une variable nommée var_test ayant pour valeur “extra var”. Que constatez-vous ? 