## Molecule : Construire un dockerfile

Ansible nécessite certains pré-requis sur les hosts cible, tels que python, et potentiellement des librairies/packages pour certains modules.

Certaines images, telles que celle utilisées jusqu’ici contiennent déjà python et permettent de tester rapidement.

Pour gérer ces pré-requis, Molecule propose un mécanisme de construction d’image docker.

L’image sera construite avant la création des instances si elle n’est pas présente dans le cache docker de la machine hôte.

La construction de l’image est contrôlée par le paramètre de l’instance pre_build_image

---

## Molecule : Exemple de Dockerfile

Le dockerfile est interprété par jinja2 avant exécution, il est donc possible d’utiliser les variables définies dans la section platform :

{% raw %}
```docker
{% if item.registry is defined %}
FROM {{ item.registry.url }}/{{ item.image }}
{% else %}
FROM {{ item.image }}
{% endif %}

{% if item.env is defined %}
{% for var, value in item.env.items() %}
{% if value %}
ENV {{ var }} {{ value }}
{% endif %}
{% endfor %}
{% endif %}

RUN apt-get update && apt-get install -y python sudo bash ca-certificates iproute2 init && apt-get clean; 
```
{% endraw %}

---

## Molecule : Paramètres de docker

Il est possible de contrôler les paramètres de démarrage des containers, pour exposer des ports, définir l’entrypoint et tout autre paramètre de ligne de commande

Exemple de customisation pour utiliser systemd dans un container :

```yaml
platforms:
  - name: instance
    image: centos:8
    command: /sbin/init
    tmpfs:
      - /run
      - /tmp
    volumes:
      - /sys/fs/cgroup:/sys/fs/cgroup:ro
```


---

## Molecule - TP6: Construire un dockerfile

![TP](./images/TP.png)

💡 Scrollez vers le bas pour les détails du TP ( 1 page )

--

## TP6

Dans ce TP, nous allons partir d’une image docker de base, et la construire pour qu’elle contienne les outils nécessaires à l'exécution d’ansible.

Dans le fichier molecule/default/molecule.yml:
* Dans la section platforms, modifier l’instance maitre pour qu’elle utilise l’image *debian:buster*

* Exécuter l’intégralité des tests : `molecule test `

L’exécution de molecule test va se terminer en erreur! en effet, cette image ne dispose pas de python, et nous ne pouvons donc pas exécuter de modules ansible dedans…

voyons comment corriger :

Dans le fichier molecule/default/molecule.yml:
* Dans la section platforms, modifier le paramétrage de l’instance maitre pour positionner à *True* le paramètre pre_build_image

* Exécuter l’intégralité des tests : `molecule test`

👍 Nous avons pu construire une image que l’on pourrait customiser selon nos besoins. Cette image a été utilisée pour l’exécution des tests, et ansible a pu y exécuter ses modules.


---

## Molecule : Dockerfile et paramètres de docker

![thinking](./images/thinking.png)

> Tous les paramètres de docker run sont disponibles via la configuration de l’instance

> Construire une image docker permet d’installer des pré-requis dans le container d’une distribution

> La construction n’est faite que si l’image n’existe pas dans le cache docker de l'hôte, il est donc possible d'accélérer les tests locaux par ce moyen